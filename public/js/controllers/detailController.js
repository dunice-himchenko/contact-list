// Generated by CoffeeScript 1.7.1
(function() {
  angular.module("android-addressbook").controller('detailCtrl', function($scope, $rootScope, $location, $routeParams, Utils, Contacts) {
    var current_route;
    current_route = "/contacts";
    $scope.selected = false;
    $scope.submenu = false;
    $scope.contact = {
      starred: false,
      firstName: "",
      lastName: "",
      birthday: "",
      picture: "",
      phones: [],
      emails: [],
      addresses: [],
      websites: [],
      notes: ""
    };
    $scope._showImage = function() {
      $scope.selected = !$scope.selected;
    };
    $scope._submenu = function() {
      $scope.submenu = !$scope.submenu;
    };
    $scope.Back = function() {
      $location.path(current_route);
    };
    $scope.ProfileImage = function(dim) {
      return ($scope.contact && $scope.contact.picture) || "https://raw.github.com/danielemoraschi/android-addressbook/master/imgs/ic_contact_picture_" + dim + ".png";
    };
    $scope.FullName = function(dim) {
      if ($scope.contact.firstName && $scope.contact.firstName.trim()) {
        return $scope.contact.firstName + " " + $scope.contact.lastName;
      } else {
        if ($scope.contact._id) {
          return "No name";
        } else {
          return "New contact";
        }
      }
    };
    $scope.StarUnStar = function() {
      $scope.contact.starred = !$scope.contact.starred;
      $scope.contact.update();
    };
    $scope.AddField = function(type) {
      $scope.contact[type] || ($scope.contact[type] = []);
      $scope.contact[type].push({
        type: "",
        value: ""
      });
    };
    $scope.DiscardField = function(type, index) {
      if ($scope.contact[type] && $scope.contact[type][index]) {
        $scope.contact[type].splice(index, 1);
      }
    };
    $scope.SaveContact = function() {
      var arrays;
      if ($scope.contact.firstName && $scope.contact.firstName.trim()) {
        arrays = {
          phones: [],
          emails: [],
          addresses: []
        };
        angular.forEach(arrays, function(v, k) {
          angular.forEach($scope.contact[k], function(val, key) {
            if (val.value.trim()) {
              arrays[k].push(val);
            }
          });
          $scope.contact[k] = arrays[k];
        });
        if ($scope.contact._id) {
          $scope.contact.update(function() {
            $location.path("/contact/view/" + $scope.contact._id.$oid);
          });
        } else {
          Contacts.save($scope.contact, function(contact) {
            $location.path("/contact/edit/" + contact._id.$oid);
          });
        }
      }
    };
    $scope.DeleteContact = (function(_this) {
      return function() {
        var c;
        if ($scope.contact._id.$oid) {
          c = confirm("Delete this contact?");
          if (c === true) {
            return _this.original["delete"](function() {
              return $location.path('/contacts');
            });
          }
        }
      };
    })(this);
    if ($routeParams.id) {
      Contacts.get({
        id: $routeParams.id
      }, (function(_this) {
        return function(contact) {
          _this.original = contact;
          if (!_this.original.views) {
            _this.original.views = 0;
          }
          _this.original.views++;
          $scope.contact = new Contacts(_this.original);
          $scope.contact.update();
          $rootScope._iScroll();
        };
      })(this));
    } else {
      $rootScope._iScroll();
    }
  });

}).call(this);
