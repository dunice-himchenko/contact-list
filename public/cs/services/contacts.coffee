angular.module("mongolab", ["ngResource"]).factory "Contacts", ($resource) ->
  Contacts = $resource("https://api.mongolab.com/api/1/databases/addressbook/collections/contacts/:id",
    apiKey: "RO27EEbdFsJfycTn_JUiAnr3qIcsgyxS"
  ,
    update:
      method: "PUT"
  )
  Contacts::update = (cb) ->
    Contacts.update
      id: @_id.$oid
    , angular.extend({}, this,
      _id: `undefined`
    ), cb

  Contacts.prototype.delete = (cb)->
    Contacts.remove
      id: @_id.$oid
      , cb
  Contacts

