angular.module("helpers", []).factory "Utils", ->
  groupify: (source, into) ->
    i = source.length - 1
    while i >= 0
      ch = source[i].firstName.charAt(0)
      into[ch] or (into[ch] =
        label: ch
        contacts: []
      )
      into[ch].contacts.push source[i]
      i--
    return
