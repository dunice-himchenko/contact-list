angular.module("android-addressbook", [
  "mongolab"
  "helpers"
]).config([
  "$routeProvider"
  "$httpProvider"
  ($routeProvider, $httpProvider, $locationProvider) ->
    $httpProvider.defaults.useXDomain = true
    $routeProvider.when("/contacts",
      templateUrl: 'views/list.html'
      controller: 'listCtrl'
    ).when("/contacts/starred",
      templateUrl: "views/starred.html"
      controller: 'listCtrl'
    ).when("/contacts/search",
      templateUrl: "views/search.html"
      controller: 'listCtrl'
    ).when("/contact/add",
      templateUrl: "views/edit.html"
      controller: 'detailCtrl'
    ).when("/contact/view/:id",
      templateUrl: "views/view.html"
      controller: 'detailCtrl'
    ).when("/contact/edit/:id",
      templateUrl: "views/edit.html"
      controller: 'detailCtrl'
    ).otherwise redirectTo: "/contacts"
]).run ($rootScope)->
  current_route = "/contacts"
  $rootScope._iScroll = ->
    iscroll and iscroll.destroy()
    iscroll = new iScroll("wrapper",
      hScroll: false
    )
    setTimeout (->
      iscroll.refresh()
      return
    ), 0
    return

