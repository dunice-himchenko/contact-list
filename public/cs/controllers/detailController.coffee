angular.module("android-addressbook").controller 'detailCtrl', ($scope, $rootScope, $location, $routeParams, Utils, Contacts) ->
  current_route = "/contacts"
  $scope.selected = false
  $scope.submenu = false
  $scope.contact =
    starred: false
    firstName: ""
    lastName: ""
    birthday: ""
    picture: ""
    phones: []
    emails: []
    addresses: []
    websites: []
    notes: ""

  $scope._showImage = ->
    $scope.selected = not $scope.selected
    return

  $scope._submenu = ->
    $scope.submenu = not $scope.submenu
    return

  $scope.Back = ->
    $location.path current_route
    return

  $scope.ProfileImage = (dim) ->
    ($scope.contact and $scope.contact.picture) or "https://raw.github.com/danielemoraschi/android-addressbook/master/imgs/ic_contact_picture_" + dim + ".png"

  $scope.FullName = (dim) ->
    (if ($scope.contact.firstName and $scope.contact.firstName.trim()) then $scope.contact.firstName + " " + $scope.contact.lastName else ((if $scope.contact._id then "No name" else "New contact")))

  $scope.StarUnStar = ->
    $scope.contact.starred = not $scope.contact.starred
    $scope.contact.update()
    return

  $scope.AddField = (type) ->
    $scope.contact[type] or ($scope.contact[type] = [])
    $scope.contact[type].push
      type: ""
      value: ""

    return

  $scope.DiscardField = (type, index) ->
    $scope.contact[type].splice index, 1  if $scope.contact[type] and $scope.contact[type][index]
    return

  $scope.SaveContact = ->
    if $scope.contact.firstName and $scope.contact.firstName.trim()
      arrays =
        phones: []
        emails: []
        addresses: []

      angular.forEach arrays, (v, k) ->
        angular.forEach $scope.contact[k], (val, key) ->
          arrays[k].push val if val.value.trim()
          return

        $scope.contact[k] = arrays[k]
        return

      if $scope.contact._id
        $scope.contact.update ->
          $location.path "/contact/view/" + $scope.contact._id.$oid
          return
      else
        Contacts.save $scope.contact, (contact) ->
          $location.path "/contact/edit/" + contact._id.$oid
          return
    return

   $scope.DeleteContact = () =>
     if $scope.contact._id.$oid
       c = confirm "Delete this contact?"
       if c is true
         @original.delete ()->
           $location.path '/contacts'

  if $routeParams.id
    Contacts.get
      id: $routeParams.id
    , (contact) =>
      @original = contact
      @original.views = 0 unless @original.views
      @original.views++
      $scope.contact = new Contacts(@original)
      $scope.contact.update()
      $rootScope._iScroll()
      return

  else
    $rootScope._iScroll()
  return

