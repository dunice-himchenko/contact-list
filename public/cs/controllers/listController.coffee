angular.module("android-addressbook").controller 'listCtrl', ($scope, $rootScope, $location, $routeParams, Utils, Contacts) ->
  $scope.orderProp = "firstName"
  $scope.groups = {}
  $scope.contacts = {}
  $scope.starred = {}
  $scope.searchterm = ""
  $scope.ProfileImage = (dim, contact) ->
    (if contact.picture then contact.picture.replace("480x480", dim) else "https://raw.github.com/danielemoraschi/android-addressbook/master/imgs/ic_contact_picture_" + dim + ".png")

  $scope.Back = ->
    $location.path current_route
    return

  switch $location.$$url
    when "/contacts/starred"
      current_route = $location.$$url
      $scope.starred = Contacts.query(
        q: "{\"starred\":true}"
      , ->
        $scope.contacts = Contacts.query(
          q: "{\"views\":{\"$gt\":0}}"
          l: 10
        , ->
          $rootScope._iScroll()
          return
        )
        return
      )
    when "/contacts/search"
      $scope.contacts = Contacts.query(->
        $scope.groups = [
          label: "All contacts"
          contacts: $scope.contacts
        ]
        $rootScope._iScroll()
        return
      )
    else
      current_route = $location.$$url
      $scope.contacts = Contacts.query(->
        Utils.groupify $scope.contacts, $scope.groups
        $rootScope._iScroll()
        return
      )
