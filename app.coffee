express = require 'express'
http = require 'http'
path = require 'path'
app = express()

allowCrossDomain = (req, res, next)->
  res.header 'Access-Control-Allow-Origin', "*"
  res.header 'Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE'
  res.header 'Access-Control-Allow-Headers', 'Content-Type'
  next();

app.configure ->
  app.set 'port', process.env.PORT or 7018
  app.use express.static(path.join(__dirname, 'public'));
  app.use allowCrossDomain

server = http.createServer(app).listen app.get('port'), ->
  console.log 'Express server listening on port ' + app.get('port')
